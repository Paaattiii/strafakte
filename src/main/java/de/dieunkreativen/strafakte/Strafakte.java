package de.dieunkreativen.strafakte;

import java.sql.SQLException;
import java.util.logging.Logger;

import de.dieunkreativen.strafakte.handler.DBHandler;
import de.dieunkreativen.strafakte.handler.PlayerEventHandler;
import de.dieunkreativen.strafakte.util.MySQL;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Strafakte extends JavaPlugin {
	Logger log;
	FileConfiguration config = getConfig();
	MySQL MySQL = new MySQL(plugin, config.getString("Host"), config.getString("Port"), config.getString("Database"), config.getString("User"), config.getString("Password"));
    public static java.sql.Connection c = null;
	PluginDescriptionFile pdfFile = this.getDescription();	
	private final PlayerEventHandler eHandler = new PlayerEventHandler(this);
	public static Strafakte plugin;

	
    @Override
    public void onLoad() {
            log = getLogger();
    }
	
	@Override
	public void onEnable() {
		plugin = this;
		
		if (!setupMySQL() ) {
            log.severe(getDescription().getName() + " Check your MySQL settings!");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
		
		saveDefaultConfig();
		PluginManager pm = this.getServer().getPluginManager();
        pm.registerEvents(eHandler, this);
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is enabled!");


	}
	@Override
	public void onDisable() {
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is disabled!");
		try {
			c.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean setupMySQL() {
		try {
			c = MySQL.openConnection();
			DBHandler.createTables();
		} catch (NullPointerException e) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public boolean onCommand(final CommandSender sender, Command cmd, String label, final String[] args) {	
		if (cmd.getName().equalsIgnoreCase("sa") && sender.hasPermission("jail.use")) {
			if(args.length > 0) {
				if(args[0].equalsIgnoreCase("delete")) {
					if (args.length >= 2) {
						if(isInteger(args[1])) {
							DBHandler.deletePunishment(args[1]);
							sender.sendMessage(ChatColor.GREEN + "Eintrag mit der ID " + args[1] + " wurde gel�scht!");
						}
						else {
							sender.sendMessage(ChatColor.RED + args[1] + " ist keine g�ltige ID!");
						}	
					}
					else {
						sender.sendMessage(ChatColor.RED + "/sa delete <ID>");
					}
				}
			}
			else {
				sender.sendMessage(ChatColor.RED + "/sa delete");
			}
		}
		return true;
	}
		
	    public boolean isInteger(String s) {
	        try {
	            Integer.parseInt(s);
	        } catch (NumberFormatException e) {
	            return false;
	        }
	        return true;
	    }
	

	

}
