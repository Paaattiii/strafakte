package de.dieunkreativen.strafakte.handler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.dieunkreativen.strafakte.Strafakte;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class DBHandler {
	
	public static void createTables() {
		
		 String tableUsers = "CREATE TABLE IF NOT EXISTS SA_Users ("
		       		+ "id int(11) NOT NULL AUTO_INCREMENT,"
		       		+ " name CHAR(20) DEFAULT NULL,"
		       		+ " uuid CHAR(50) DEFAULT NULL,"
		       		+ " PRIMARY KEY (id))";
		
		String punishedUsers = "CREATE TABLE IF NOT EXISTS SA_punishedUsers ("
	          		+ "punishID int(11) NOT NULL AUTO_INCREMENT,"
	          		+ " criminalUserID CHAR(50) DEFAULT NULL,"
	          		+ " teamMemberID CHAR(50) DEFAULT NULL,"
	          		+ " punishment CHAR(10) DEFAULT NULL,"
	          		+ " reason CHAR(20) DEFAULT NULL,"
	          		+ " duration CHAR(10) DEFAULT NULL,"
	          		+ " dtime DATETIME DEFAULT NULL,"
	          		+ " PRIMARY KEY (punishID))";

	        try
	        {
	        	java.sql.Statement statement = Strafakte.c.createStatement();
	        	statement.executeUpdate(tableUsers);
	        	statement.executeUpdate(punishedUsers);
	        	statement.close();
	        }
	        catch (SQLException e)
	        {
	        	e.printStackTrace();            
	        }
	}
	
	public static void addPunishment(String criminalUser, String teammember, String punishment, String reason, String duration) {
	    try {
	    		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	    		Date date = new Date();
	    		String time = dateFormat.format(date);
	    		if (!duration.equalsIgnoreCase("-")) {
	    			duration = duration + " Minuten";
	    		}
	    		java.sql.Statement statement = Strafakte.c.createStatement();
	    		statement.executeUpdate("INSERT INTO SA_punishedUsers "
	    				+ "(`criminalUserID`,  `teamMemberID`, `punishment`, `reason`, `duration`, `dtime`) "
	    				+ "VALUES ('" + getID(criminalUser) + "', '" + getID(teammember) + "', '" + punishment + "', "
	    				+ "'" + reason + "', '" + duration + "', '" + time + "');");
	    		statement.close();
			} catch (SQLException ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
		}
	}
	
	public static void updateUsers (Player p) {
		String uuid = p.getUniqueId().toString();
		String player = p.getName();
	    //User existiert noch nicht in der Datenbank
		if (getID(player) == 0) {
			try {
				java.sql.Statement statement = Strafakte.c.createStatement();
				statement.executeUpdate("INSERT INTO SA_Users (`name`, `uuid`) VALUES ('" + player + "', '" + uuid + "');");
				statement.close();
			} catch (SQLException ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}
		}
		//User existiert bereits -> Namen aktualisieren
		else {
			try {
				String query = "UPDATE SA_Users SET name = ? WHERE UUID = ?";
			 	java.sql.PreparedStatement preparedStmt = Strafakte.c.prepareStatement(query);
				preparedStmt.setString   (1, player);
				preparedStmt.setString(2, uuid);
			 
				preparedStmt.executeUpdate();
				preparedStmt.close();
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	
	@SuppressWarnings("deprecation")
	public static int getID(String name) {
		int id = 0;
		try {
			java.sql.Statement st = Strafakte.c.createStatement();
			String sql = ("SELECT id FROM SA_Users WHERE uuid= '" + Bukkit.getOfflinePlayer(name).getUniqueId().toString() + "';");
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				id = rs.getInt("id"); 
			}
			rs.close();
			st.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
		return id;
	}
	
	public static void deletePunishment(String id) {
		try {
			java.sql.Statement statement = Strafakte.c.createStatement();
			statement.executeUpdate("DELETE FROM SA_punishedUsers WHERE punishID =" + id);
			statement.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}
	

}
