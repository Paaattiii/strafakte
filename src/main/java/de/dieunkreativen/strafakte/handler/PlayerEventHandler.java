package de.dieunkreativen.strafakte.handler;

import de.dieunkreativen.strafakte.Strafakte;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerEventHandler implements Listener  {
	Strafakte plugin;
	public PlayerEventHandler(Strafakte instance) {
		plugin = instance;
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e){
		DBHandler.updateUsers(e.getPlayer());
	}
	
	@EventHandler
	public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent e){
		String[] command = e.getMessage().split(" ");
		
		  if(command[0].equalsIgnoreCase("/jail") && e.getPlayer().hasPermission("jail.use")) {
			  if (command.length >= 4) {
					if(isInteger(command[2])) {
						DBHandler.addPunishment(command[1], e.getPlayer().getName(), "Gef�ngnis", parseReason(command, 3), command[2]);
					}
			  }
		  }
		  else if(command[0].equalsIgnoreCase("/kick") && e.getPlayer().hasPermission("jail.use")) {
			  if (command.length >= 3) {
				  DBHandler.addPunishment(command[1], e.getPlayer().getName(), "Serverkick", parseReason(command, 2), "-");
			  }
			  else {
				  e.getPlayer().sendMessage(ChatColor.DARK_RED + "/kick <Spieler> <Grund>");
				  e.setCancelled(true);
			  }
		  }
		  else if(command[0].equalsIgnoreCase("/mute") && e.getPlayer().hasPermission("jail.use")) {
			  if (command.length >= 4) {
				  DBHandler.addPunishment(command[1], e.getPlayer().getName(), "Stumm geschalten", parseReason(command, 3), command[2]);
			  }
			  else {
				  e.getPlayer().sendMessage(ChatColor.DARK_RED + "/mute <Spieler> <Zeit in Sekunden> <Grund>");
				  e.setCancelled(true);
			  }
		  }
		  
			    
	}
	
	public String parseReason (String [] args, int limit) {
		StringBuilder buffer = new StringBuilder();

		// change the starting i value to pick what argument to start from
		// 1 is the 2nd argument.
		for(int i = limit; i < args.length; i++)
		{
		    buffer.append(' ').append(args[i]);
		}
		
		return buffer.toString();
	}
	
    public boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

}
